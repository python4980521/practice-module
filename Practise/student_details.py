import psycopg2
from psycopg2 import sql

def get_student_details():
    student_id = input("Enter student ID: ")
    student_name = input("Enter student's name: ")
    student_age = input("Enter student's age: ")
    student_major = input("Enter student's major: ")
    college_id = input("Enter the college ID for the student: ")
    
    return {
        'id': student_id,
        'name': student_name,
        'age': student_age,
        'major': student_major,
        'college_id': college_id
    }

def get_college_details():
    college_id = input("Enter college ID: ")
    college_name = input("Enter college name: ")
    college_address = input("Enter college address: ")
    college_contact = input("Enter college contact number: ")
    
    return {
        'id': college_id,
        'name': college_name,
        'address': college_address,
        'contact': college_contact
    }

def insert_student_details(conn, student_details):
    with conn.cursor() as cur:
        # Check if the college ID exists
        cur.execute(
            sql.SQL("SELECT 1 FROM colleges WHERE id = %s"),
            (student_details['college_id'],)
        )
        if cur.fetchone() is None:
            print(f"Error: College ID {student_details['college_id']} does not exist. Please insert college details first.")
            return

        # Insert student details
        cur.execute(
             sql.SQL("INSERT INTO students (id, name, age, major, college_id) VALUES (%s, %s, %s, %s, %s)"),
            (student_details['id'], student_details['name'], student_details['age'], student_details['major'], student_details['college_id'])
        )
        conn.commit()
        print("Student details inserted successfully!")

def insert_college_details(conn, college_details):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("INSERT INTO colleges (id, name, address, contact) VALUES (%s, %s, %s, %s)"),
            (college_details['id'], college_details['name'], college_details['address'], college_details['contact'])
        )
        conn.commit()
        print("College details inserted successfully!")

def select_student_details(conn, student_id):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("SELECT s.id, s.name, s.age, s.major, c.name FROM students s JOIN colleges c ON s.college_id = c.id WHERE s.id = %s"),
            (student_id,)
        )
        student_record = cur.fetchone()
        if student_record:
            print(f"Student ID: {student_record[0]}")
            print(f"Name: {student_record[1]}")
            print(f"Age: {student_record[2]}")
            print(f"Major: {student_record[3]}")
            print(f"College: {student_record[4]}")
        else:
            print("Student not found.")

def select_college_details(conn, college_id):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("SELECT * FROM colleges WHERE id = %s"),
            (college_id,)
        )
        college_record = cur.fetchone()
        if college_record:
            print(f"College ID: {college_record[0]}")
            print(f"Name: {college_record[1]}")
            print(f"Address: {college_record[2]}")
            print(f"Contact: {college_record[3]}")
        else:
            print("College not found.")

def delete_student_details(conn, student_id):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("SELECT 1 FROM students WHERE id = %s"),
            (student_id,)
        )
        if cur.fetchone() is None:
            print(f"Error: Student ID {student_id} does not exist.")
            return
        
        cur.execute(
            sql.SQL("DELETE FROM students WHERE id = %s"),
            (student_id,)
        )
        conn.commit()
        print(f"Deleted student record with ID {student_id}")

def delete_college_details(conn, college_id):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("SELECT 1 FROM colleges WHERE id = %s"),
            (college_id,)
        )
        if cur.fetchone() is None:
            print(f"Error: College ID {college_id} does not exist.")
            return
        
        cur.execute(
            sql.SQL("DELETE FROM colleges WHERE id = %s"),
            (college_id,)
        )
        conn.commit()
        print(f"Deleted college record with ID {college_id}")

def truncate_table(conn, table_name):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("TRUNCATE TABLE {} CASCADE").format(sql.Identifier(table_name))
        )
        conn.commit()
        print(f"All data from table {table_name} has been truncated.")

def delete_all_data(conn, table_name):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("DELETE FROM {}").format(sql.Identifier(table_name))
        )
        conn.commit()
        print(f"All data from table {table_name} has been deleted.")
def reset_sequence(conn, table_name, column_name):
    with conn.cursor() as cur:
        cur.execute(
            sql.SQL("SELECT setval(pg_get_serial_sequence(%s, %s), 1, false)"),
            (sql.Identifier(table_name), sql.Identifier(column_name))
        )
        conn.commit()
        print(f"Sequence for column {column_name} in table {table_name} reset successfully.")

def display_menu():
    print("\nChoose an action:")
    print("1. Insert student details")
    print("2. Insert college details")
    print("3. Select student details")
    print("4. Select college details")
    print("5. Delete student details")
    print("6. Delete college details")
    print("7. Truncate students table")
    print("8. Truncate colleges table")
    print("9. Delete all data from students table")
    print("10. Delete all data from colleges table")
    print("11. Exit")
    return input("Enter the number of your choice: ")

def create_tables(conn):
    with conn.cursor() as cur:
        cur.execute("""
        CREATE TABLE IF NOT EXISTS colleges (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            address TEXT NOT NULL,
            contact VARCHAR(50) NOT NULL
        );
        """)
        
        cur.execute("""
        CREATE TABLE IF NOT EXISTS students (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            age INT NOT NULL,
            major VARCHAR(255) NOT NULL,
            college_id INT REFERENCES colleges(id) ON DELETE CASCADE
            );
            """)
        
        conn.commit()
        print("Tables created successfully!")

def main():
    conn_params = {
        'dbname': 'student_college_db',
        'user': 'postgres',
        'password': '1234',
        'host': 'localhost',
        'port': '5432'
    }

    conn = psycopg2.connect(**conn_params)

    # Create tables
    create_tables(conn)

    while True:
        choice = display_menu()

        if choice == '1':
            print("\nEnter student details:")
            student_details = get_student_details()
            insert_student_details(conn, student_details)

        elif choice == '2':
            print("\nEnter college details:")
            college_details = get_college_details()
            insert_college_details(conn, college_details)

        elif choice == '3':
            student_id = input("\nEnter the student ID to fetch details: ")
            select_student_details(conn, student_id)

        elif choice == '4':
            college_id = input("\nEnter the college ID to fetch details: ")
            select_college_details(conn, college_id)

        elif choice == '5':
            student_id_to_delete = input("\nEnter the student ID to delete: ")
            delete_student_details(conn, student_id_to_delete)

        elif choice == '6':
            college_id_to_delete = input("\nEnter the college ID to delete: ")
            delete_college_details(conn, college_id_to_delete)

        elif choice == '7':
            truncate_table(conn, 'students')

        elif choice == '8':
            truncate_table(conn, 'colleges')

        elif choice == '9':
            delete_all_data(conn, 'students')

        elif choice == '10':
            delete_all_data(conn, 'colleges')

        elif choice == '11':
            conn.close()
            print("Exiting...")
            break

if __name__ == "__main__":
    main()
